#+TITLE: Jour 15
#+DATE: 2014-07-23T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day15.jpg
#+PLACE: Grimsey
#+SUMMARY: 

** Etape 

Grimsey

** Résumé

Génial. Super temps (soleil toute la journée). Super coin (falaises et vues
sur les fjords d'Islande). Mode National Geographic activé (moutons, chevaux,
etc). Plein de macareux moines (trop pour Mathieu). Bain au nord du cercle
polaire : Check ! Horribles, horribles sternes arctiques menaçants. Mathieu ne
s'est pas plaint de la journée !

{{< figure src="/islande/media/photos/day15/page30-31.jpg" class="image-article" width="75%">}}
