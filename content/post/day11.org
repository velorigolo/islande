#+TITLE: Jour 11 
#+DATE: 2014-07-19T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day11.jpg
#+PLACE: Myvatn
#+SUMMARY: 

** Etape 

Myvatn (lac)

** Kilomètres

25

** Résumé

Myvatn est magnifique. Je n'a rien à dire quand on fait pas de vélo. À part
que Mathieu est (encore) un rat et en plus il se fait tirer à vélo. Acheté une
boule de graisse et de sang de mouton 'Spécialité' infecte. Bains volcaniues
de nuit dans l'eau souffrée sous la pluie : magique

{{< figure src="/islande/media/photos/day11/page22-23.jpg" class="image-article" width="75%">}}
